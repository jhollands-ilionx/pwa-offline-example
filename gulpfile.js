var gulp = require('gulp');
var replace = require('gulp-replace');
var rename = require('gulp-rename');
var header = require('gulp-header');
var footer = require('gulp-footer');

gulp.task('production', function () {
  gulp.start('add-raw-tag');
  gulp.start('copy-and-rename-index');
  gulp.start('replace-tags');
  gulp.start('replace-tags-ngsw');
});

gulp.task('add-raw-tag', function () {
  return gulp
    .src('dist/**/*.js')
    .pipe(header('{%raw%}'))
    .pipe(footer('{%endraw%}'))
    .pipe(gulp.dest('dist'));
});

gulp.task('copy-and-rename-index', function () {
  return gulp
    .src('dist/pwa-offline/index.html')
    .pipe(rename('index'))
    .pipe(gulp.dest('dist/pwa-offline'));
});

gulp.task('replace-tags', function () {
  return gulp
    .src('dist/pwa-offline/index.html')
    .pipe(replace('main.js', "{% javascript_url 'main' %}"))
    .pipe(replace('polyfills-es5.js', "{% javascript_url 'polyfills-es5' %}"))
    .pipe(replace('polyfills.js', "{% javascript_url 'polyfills' %}"))
    .pipe(replace('runtime.js', "{% javascript_url 'runtime' %}"))
    .pipe(replace('styles.css', "{% stylesheet_url 'styles' %}"))
    .pipe(gulp.dest('dist/pwa-offline'));
});

gulp.task('replace-tags-ngsw', function () {
  return gulp
    .src('dist/pwa-offline/ngsw.json')
    .pipe(
      replace(
        '/assets/icons/icon-72x72.png',
        'https://assets.bettyblocks.com/311b2b0203e9435e879e107d9cb67ef0_assets/files/icon-72x72'
      )
    )
    .pipe(
      replace(
        '/assets/icons/icon-96x96.png',
        'https://assets.bettyblocks.com/311b2b0203e9435e879e107d9cb67ef0_assets/files/icon-96x96'
      )
    )
    .pipe(
      replace(
        '/assets/icons/icon-128x128.png',
        'https://assets.bettyblocks.com/311b2b0203e9435e879e107d9cb67ef0_assets/files/icon-128x128'
      )
    )
    .pipe(
      replace(
        '/assets/icons/icon-144x144.png',
        'https://assets.bettyblocks.com/311b2b0203e9435e879e107d9cb67ef0_assets/files/icon-144x144'
      )
    )
    .pipe(
      replace(
        '/assets/icons/icon-152x152.png',
        'https://assets.bettyblocks.com/311b2b0203e9435e879e107d9cb67ef0_assets/files/icon-152x152'
      )
    )
    .pipe(
      replace(
        '/assets/icons/icon-192x192.png',
        'https://assets.bettyblocks.com/311b2b0203e9435e879e107d9cb67ef0_assets/files/icon-192x192'
      )
    )
    .pipe(
      replace(
        '/assets/icons/icon-384x384.png',
        'https://assets.bettyblocks.com/311b2b0203e9435e879e107d9cb67ef0_assets/files/icon-384x384'
      )
    )
    .pipe(
      replace(
        '/assets/icons/icon-512x512.png',
        'https://assets.bettyblocks.com/311b2b0203e9435e879e107d9cb67ef0_assets/files/icon-512x512'
      )
    )
    .pipe(gulp.dest('dist/pwa-offline'));
});
