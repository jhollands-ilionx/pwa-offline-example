import { Component, OnInit } from '@angular/core';
import { SwUpdate, UpdateAvailableEvent } from '@angular/service-worker';
import { Store } from '@ngxs/store';
import { GetWeatherInfo } from 'src/offline/state/offline-state.actions';
import { SubscriptionComponent } from 'src/shared/helpers/subscription.helper';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends SubscriptionComponent implements OnInit {
  title = 'pwa-offline';

  constructor(private _store: Store, private _swUpdate: SwUpdate) {
    super()
  }

  ngOnInit(): void {
    this.checkForNewVersion();
  }

  testRequest(): void {
    this._store.dispatch(new GetWeatherInfo());
  }

  checkForNewVersion(): void {
    this.addSub(
      this._swUpdate.available.subscribe((event: UpdateAvailableEvent) => {
        if (window.confirm('Nieuwe versie beschikbaar!')) {
          this._swUpdate.activateUpdate().then(() => document.location.reload());
        }
      })
    );
  }
}