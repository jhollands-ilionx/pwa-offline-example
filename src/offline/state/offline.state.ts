import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { SharedService } from 'src/shared/services/shared.service';

import { SubscriptionComponent } from '../../shared/helpers/subscription.helper';
import DynamicClass from '../dynamic-class-helper';
import { ActionToDispatch } from '../models/action-to-dispatch.model';

import { AddActionToDispatch, GetWeatherInfo, PostActionsToDispatch } from './offline-state.actions';
import { OfflineStateModel } from './offline-state.model';

@State<OfflineStateModel>({
  name: 'offline',
  defaults: {
    actionsToDispatch: [],
    dispatchingActions: false
  },
})
@Injectable()
export class OfflineState extends SubscriptionComponent {
  constructor(private _sharedService: SharedService) {
    super()
  }

  @Selector()
  static actionsToDispatch(state: OfflineStateModel): ActionToDispatch[] {
    return state.actionsToDispatch;
  }

  /**
   * Add a actions to dispatch later
   *
   * @param {StateContext<OfflineStateModel>} { getState, patchState }
   * @param {AddActionToDispatch} { type, params }
   * @memberof OfflineState
   */
  @Action(AddActionToDispatch)
  AddActionToDispatch({ getState, patchState }: StateContext<OfflineStateModel>, { type, params }: AddActionToDispatch): void {
    const actionsToDispatch: ActionToDispatch[] = getState().actionsToDispatch;

    const newActionToDispatch: ActionToDispatch = {
      type,
      params
    }

    patchState({ actionsToDispatch: [...actionsToDispatch, newActionToDispatch] })
  }

  /**
   * Post all actions that were not able to dispatch when offline
   *
   * @param {StateContext<OfflineStateModel>} { patchState }
   * @returns {Observable<any>}
   * @memberof OfflineState
   */
  @Action(PostActionsToDispatch)
  postActionsToDispatch({ getState, patchState, dispatch }: StateContext<OfflineStateModel>): void {
    if (this._sharedService.online$.value === true) {
      const actionsToDispatch: ActionToDispatch[] = getState().actionsToDispatch;
      if (actionsToDispatch.length) {
        patchState({ dispatchingActions: true });
        const actions: DynamicClass[] = actionsToDispatch.map(action => new DynamicClass(action.type, action.params));

        this.addSub(
          dispatch(actions).subscribe(() => {
            patchState({ dispatchingActions: false, actionsToDispatch: [] });
          }, (error) => {
            patchState({ dispatchingActions: false, actionsToDispatch: [] });
            throw error;
          })
        );
      }
    }
  }

  /**
   * EXAMPLE ACTION
   *
   * @param {StateContext<OfflineStateModel>} ctx
   * @returns {Observable<any>}
   * @memberof OfflineState
   */
  @Action(GetWeatherInfo)
  getWeatherInfo(ctx: StateContext<OfflineStateModel>): Observable<any> {
    if (this._sharedService.online$.value === true) {
      return this._sharedService.getWeatherInfo().pipe(
        tap((info) => {
          console.log(info);
        }),
        catchError(err => {
          console.error(err)
          throw err;
        })
      )
    } else {
      ctx.dispatch(new AddActionToDispatch('GetWeatherInfo', []));
      console.log('you are offline, this will run when user gets back online');
    }
  }
}
