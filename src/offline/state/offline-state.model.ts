import { ActionToDispatch } from '../models/action-to-dispatch.model';

export class OfflineStateModel {
  actionsToDispatch: ActionToDispatch[];
  dispatchingActions: boolean;
}
