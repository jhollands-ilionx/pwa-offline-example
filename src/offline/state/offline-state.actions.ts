export class AddActionToDispatch {
  static readonly type = '[Offline] Add a action to the array';
  constructor(public type: string, public params: any[]) { }
}

export class PostActionsToDispatch {
  static readonly type = '[Offline] Post all actions that were not able to dispatch when offline';
}

export class GetWeatherInfo {
  static readonly type = '[Offline] - example of a request'
}


