export interface ActionToDispatch {
    type: string;
    params: any[];
}