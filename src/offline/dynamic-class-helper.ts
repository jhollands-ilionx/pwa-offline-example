import { GetWeatherInfo } from './state/offline-state.actions';

const classes = {
    GetWeatherInfo
};

class DynamicClass {
    constructor(className: string, opts: any[]) {
        return new classes[className](...opts);
    }
}

export default DynamicClass;

