import { OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

export class SubscriptionComponent implements OnDestroy {
  subs: Subscription[] = [];

  addSub(sub: Subscription): void {
    this.subs.push(sub);
  }

  ngOnDestroy(): void {
    this.subs.forEach(s => s.unsubscribe());
    this.subs = [];
  }
}
