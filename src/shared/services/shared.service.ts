import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { ConnectionService } from 'ng-connection-service';
import { BehaviorSubject, Observable } from 'rxjs';
import { PostActionsToDispatch } from 'src/offline/state/offline-state.actions';

import { SubscriptionComponent } from '../helpers/subscription.helper';

@Injectable({
  providedIn: 'root',
})
export class SharedService extends SubscriptionComponent {
  online$: BehaviorSubject<boolean> = new BehaviorSubject(navigator.onLine);

  private api_url = 'https://api.openweathermap.org/data/2.5/weather?q=London&appid=787954e4ebc5078bd196f23dcbedbb97'

  constructor(private _store: Store, private _connectionService: ConnectionService, private _http: HttpClient) {
    super()
    this.addSub(
      this._connectionService.monitor().subscribe(event => {
        this.online$.next(event);

        if (event === true) {
          this.checkForActionsToDispatch();
        }
      })
    )
  }

  checkForActionsToDispatch(): void {
    this._store.dispatch(new PostActionsToDispatch())
  }

  getWeatherInfo(): Observable<any> {
    return this._http.get<any>(this.api_url);
  }
}
